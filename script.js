
var globals = {
  response_parsed: {},
  xhr: new XMLHttpRequest()
}

globals.xhr.addEventListener("readystatechange", reqListener, false) //listening for the readystatechange property to be changed

function new_quote_pull () {

  globals.xhr.open('GET', 'http://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=1', true) //last value says "run this request async"
  globals.xhr.send()

  globals.xhr.onreadystatechange = reqListener // I don't understand line 14. I'd think reqListener should take an object (e) and then mutate it.

  reqListener()
}

function reqListener () {

  if (globals.xhr.readyState === 4 && globals.xhr.status === 200) {
    globals.response_parsed = JSON.parse(globals.xhr.responseText) //parsing turns a long string into an object

    update_quote_div()
  }
}

function update_quote_div() {
  // if (globals.xhr.readyState === 4 && globals.xhr.status === 200) {
  var quote_from_api = globals.response_parsed[0].content
  console.log(globals.response_parsed[0].content)
  console.log(quote_from_api)

  var quote_div = document.getElementById("quote-box")

  quote_div.innerHTML = quote_from_api
  // }
}


